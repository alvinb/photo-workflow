const path = require('path');

const rawExtensions = ['.arw', '.cr2'];

const isRaw = fileName => {
  const ext = path.extname(fileName).toLowerCase();
  return rawExtensions.includes(ext);
};

module.exports = isRaw;
