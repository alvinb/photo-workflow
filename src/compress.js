const fg = require('fast-glob');
const path = require('path');
const resize = require('./resize');
const isRaw = require('./isRaw');
const execp = require('./execp');

const compress = async (files, inDir, outDir) => {
  for (file of files) {
    const baseName = path.basename(file);
    let outFile = `${outDir}/${baseName}`;

    console.log('Processing', baseName);

    // convert to jpeg first
    if (isRaw(file)) {
      console.log('This is a raw file, converting to JPEG');

      const fileNameNoExt = getFileName(file, true);

      outFile = `${outDir}/${fileNameNoExt}.jpg`;
      const inFile = `${inDir}/${fileNameNoExt}.jpg`;

      await execp(`sips -s format jpeg ${file} --out "${inFile}"`);

      await resize(inFile, outFile);
    } else {
      await resize(file, outFile);
    }

    console.log('Done!');
  }
};

// convert raw : for i in *.CR2; do sips -s format jpeg $i --out "${i%.*}.jpg"; done

const getFileName = (fileName, extension = false) =>
  extension
    ? path.basename(fileName, path.extname(fileName))
    : path.basename(file);

module.exports = compress;
